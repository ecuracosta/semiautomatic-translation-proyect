import requests
from bs4 import BeautifulSoup
from deep_translator import GoogleTranslator # Try also PonsTranslator, LingueeTranslator, MyMemoryTranslator
import deep_translator.exceptions as dt
import time
import os

def squarespace(url,language,requestsCount):
    # Page to parse
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "lxml")

    # Removing anoying elements
    removals = soup.find_all('footer')
    for match in removals:
        match.decompose()

    print("    Translating",url)
    # Iterates over squarespace content blocks
    for div_class in soup.find_all('div', {'class':'sqs-block-content'}):
        for tag in div_class.find_all(['h1','h2','h3','p']):
            if tag.text:
            # Only tags that contain text
                try:
                    if not tag.text.isspace():
                    # Translate if there is text inside the tag
                        #print("requestsCount:",requestsCount)
                        tag.string.replace_with(GoogleTranslator(source='english', target=language).translate(text=tag.string))
                        requestsCount[0] += 1
                        if not None == tag.string:
                            requestsCount[1] += len(tag.string)
                        if requestsCount[0]%500 == 0:
                            print("Requests per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                            time.sleep(60)
                            print("Resuming the job!")
                        elif requestsCount[1]%5000000 == 0:
                            print("Characters per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                            time.sleep(60)
                            print("Resuming the job!")
                except (dt.NotValidLength,dt.TranslationNotFound) as e:
                # Captures error if translation not possible keeping the text in english
                    pass
                except AttributeError:
                # If there are childs
                    for child in tag.contents:
                        try:
                            if not tag.text.isspace():
                                #print("requestsCount:",requestsCount)
                                child.string.replace_with(' '+GoogleTranslator(source='english', target=language).translate(text=child.string)+' ')
                                requestsCount[0] += 1
                                if not None == child.string:
                                    requestsCount[1] += len(child.string)
                                if requestsCount[0]%500 == 0:
                                    print("Requests per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                                    time.sleep(60)
                                    print("Resuming the job!")
                                elif requestsCount[1]%5000000 == 0:
                                    print("Characters per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                                    time.sleep(60)
                                    print("Resuming the job!")
                        except (dt.NotValidLength,dt.TranslationNotFound) as e:
                            pass
                        except AttributeError:
                            # If there still childs
                            for grandchild in child.contents:
                                try:
                                    if not tag.text.isspace():
                                        #print("requestsCount:",requestsCount)
                                        grandchild.string.replace_with(' '+GoogleTranslator(source='english', target=language).translate(text=grandchild.string)+' ')
                                        requestsCount[0] += 1
                                        if not None == grandchild.string:
                                            requestsCount[1] += len(grandchild.string)
                                        if requestsCount[0]%500 == 0:
                                            print("Requests per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                                            time.sleep(60)
                                            print("Resuming the job!")
                                        elif requestsCount[1]%5000000 == 0:
                                            print("Characters per minute exceed the the Google quota. Waiting 1 minute to avoid IP ban...")
                                            time.sleep(60)
                                            print("Resuming the job!")
                                except (dt.NotValidLength,dt.TranslationNotFound) as e:
                                    pass
                                except AttributeError:
                                    # If there still childs raise an error!
                                    if grandchild.text:
                                        raise Exception("Still things to translate inside the grandchild!")


    url_tail_list = url[31:].split('/')
    for folder in url_tail_list[:-1]:
        if not os.path.exists(folder):
            os.mkdir(folder)
        os.chdir(folder)
    with open(url_tail_list[-1]+".html", "w") as file:
        file.write(str(soup))
    for i in range(0,len(url_tail_list[:-1])):
        os.chdir('..')

    return requestsCount
