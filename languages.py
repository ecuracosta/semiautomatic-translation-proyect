
from deep_translator import GoogleTranslator

langs_list = GoogleTranslator.get_supported_languages()
print('\n'.join(langs_list))
