import time
from linkering import *
from squarespace import *
from hierarchy import *

# Parameters
url = 'https://www.endcoronavirus.org/'
languageList = ['spanish']

requestsCount = [1,1] # [request,characters]
for language in languageList:

    path = './'+str(language)
    if not os.path.exists(path):
        os.mkdir('./'+str(path))
    os.chdir('./'+str(path))

    directories_hierarchy = get_links(url,squarespace,language,requestsCount)

    print("")
    print("Folders structure:")
    print("")

    print_tree(directories_hierarchy)
