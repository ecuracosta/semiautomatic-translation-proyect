from anytree import Node, RenderTree

def print_tree(luca):
# Recibes the main node (luca) and print the directories hierarchy
    for pre, fill, node in RenderTree(luca):
        print("%s%s" % (pre, node.name))
