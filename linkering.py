from bs4 import BeautifulSoup
import requests
import requests.exceptions
from urllib.parse import urlsplit
from urllib.parse import urlparse
from collections import deque
import os
from anytree import Node, RenderTree

def get_links(url,template_translation,language,requestsCount):

    root=url

    # a queue of urls to be crawled next
    newUrls = deque([url])
    # a set of urls that we have already processed
    processedUrls = set()
    # a set of urls that we have not processed
    #non_processedUrls = set()
    # a set of domains inside the target website
    localUrls = set()
    # a set of domains outside the target website
    foreignUrls = set()
    # a set of broken urls
    brokenUrls = set()
    # a set of desire urls to translate
    toTranslate = set()

    nodes = {root:Node(url)}
    parent = nodes[url]

    # process urls one by one until we exhaust the queue
    while len(newUrls):
        # move url from the queue to processed url set
        url = newUrls.popleft()
        processedUrls.add(url)
        # print the current url:
        print("Processing %s" % url)
        try:
            response = requests.get(url)
        except(requests.exceptions.MissingSchema, requests.exceptions.ConnectionError, requests.exceptions.InvalidURL, requests.exceptions.InvalidSchema):
            # add broken urls to it’s own set, then continue
            brokenUrls.add(url)
            continue
        # extract base url to resolve relative links
        parts = urlsplit(url)
        base = "{0.netloc}".format(parts)
        stripBase = base.replace("www.", "")
        baseUrl = "{0.scheme}://{0.netloc}".format(parts)
        path = url[:url.rfind('/')+1] if '/' in parts.path else url
        soup = BeautifulSoup(response.text, "lxml")
        for link in soup.find_all('a'):
            # extract link url from the anchor
            anchor = link.attrs["href"] if "href" in link.attrs else ''
            if anchor.startswith('/'):
                localLink = baseUrl + anchor
                localUrls.add(localLink)
                toTranslate.add(localLink)
                new_file = localLink
            elif stripBase in anchor:
                localUrls.add(anchor)
            elif not anchor.startswith('http'):
                localLink = path + anchor
                localUrls.add(localLink)
            else:
                foreignUrls.add(anchor)
        for i in toTranslate:
            if not i in newUrls and not i in processedUrls and '#' not in i and not i.endswith('.pdf'):
                newUrls.append(i)
                nodes[i] = Node(i,parent=parent)
                requestsCount = template_translation(i,language,requestsCount)

    return nodes[root]
