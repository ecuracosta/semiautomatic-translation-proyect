# Semiautomatic translation proyect

The whole idea is to have a web scraper working decently, that separates pages by blocks and then translate them to different languages.

Having trouble with the translation quota. The rest seems to work.

###### To Do:

* Findout how to automatically update the html outputs to squarespace.

* A Slack bot that, when you talk him, sends you one block to check. You should respond him with the corrections (that would end up in a database). The database could contains all the blocks corrections, who did it, when and so on (we can have a tracing of the process).